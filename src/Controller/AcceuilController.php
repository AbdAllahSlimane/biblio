<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Livre;
use App\Repository\EmpruntRepository;
use App\Repository\LivreRepository;
use Symfony\Component\HttpFoundation\Request;

class AcceuilController extends AbstractController
{
    /**
     * @Route("/", name="acceuil")
     */
    public function index(LivreRepository $lr, EmpruntRepository $empruntRepository)
    {
        $repository = $this->getDoctrine()->getRepository(Livre::class);
        $liste_livre = $repository->findAll();
        return $this->render('base.html.twig', [
            'liste_livres' => $liste_livre,
            'ChloeEmprunt' => $empruntRepository->findByChloeEmprunt()

        ]);
    }

    /**
     * @Route("/exo", name="exo")
     */
    public function exo(Request $request,EmpruntRepository $empruntRepository)
    {
        $auteur = $request->query->get('prenom');
        $liste_prenoms = 
        $empruntRepository->findByAlphonseEmprunt($auteur);
        //------------------------------
        $prenom =  $request->query->get('prenom');
        if($prenom){
            $liste_livres = $empruntRepository->findByNameEmprunt($prenom); 
        }else{
            $liste_livres = [];
        }
        return $this->render('acceuil/index.html.twig', [
            'ChloeEmprunt' => $empruntRepository->findByChloeEmprunt(),
            'liste_livres' => $liste_livres,
            'prenom' => $prenom,
            'liste_prenoms' => $liste_prenoms

        ]);
    }

    /**
     * @Route("/recherche", name="recherche")
     */
    public function recherche(Request $request, LivreRepository $livreRepository)
    {
       $mot = $request->query->get("mot_recherche");
       if($mot){
       $liste_livres = $livreRepository->findBySearch($mot);
        }else{
            $liste_livres = [];
        }
       return $this->render('base.html.twig', [
           'liste_livres' => $liste_livres,
           'mot' => $mot
       ]);
    }
}
