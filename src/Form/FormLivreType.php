<?php

namespace App\Form;

use App\Entity\Livre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

class FormLivreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('auteur', TextType::class,[
                'constraints' => [
                    new Constraints\Length([
                        'max' => 20,
                        'maxMessage' => "Le nom de l'auteur doit contenir {{ limit }} caractères"
                    ])
                ]
            ]) 
            ->add('titre', TextType::class,[
                'constraints' => [
                    new Constraints\Length([
                        'max' => 50,
                        'maxMessage' => "Le titre du livre doit contenir {{ limit }} caractères"
                    ])
                ]
            ]) 
            ->add('submit',SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Livre::class,
        ]);
    }
}
