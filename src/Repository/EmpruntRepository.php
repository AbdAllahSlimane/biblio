<?php

namespace App\Repository;

use App\Entity\Emprunt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Emprunt|null find($id, $lockMode = null, $lockVersion = null)
 * @method Emprunt|null findOneBy(array $criteria, array $orderBy = null)
 * @method Emprunt[]    findAll()
 * @method Emprunt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmpruntRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Emprunt::class);
    }

    // /**
    //  * @return Emprunt[] Returns an array of Emprunt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function findByNonRendu()
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.date_rendu IS NULL')
            ->orderBy('l.date_sortie', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    public function findByNonEmprunte()
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.date_sortie IS NULL')
            ->getQuery()
            ->getResult()
        ;
    }
    public function findByChloeEmprunt()
    {
        return $this->createQueryBuilder('e')
            ->join('e.abonne', 'a')
            ->Where('a.prenom = :val')
            ->setParameter('val', "Chloe")
            ->orderBy('e.id', 'ASC')
            ->orderBy('e.date_sortie', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    public function findByNameEmprunt($prenom)
    {
        return $this->createQueryBuilder('e')
            ->join('e.abonne', 'a')
            ->Where('a.prenom = :val')
            ->setParameter('val', $prenom)
            ->orderBy('e.id', 'ASC')
            ->orderBy('e.date_sortie', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    
    public function findByAlphonseEmprunt($auteur)
    {
        return $this->createQueryBuilder('e')
            ->join('e.livre', 'a')
            ->Where('a.auteur = :val')
            ->setParameter('val', $auteur)
            ->orderBy('e.id', 'ASC')
            ->orderBy('e.date_sortie', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Emprunt
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
